#!/usr/bin/env python
#-- Copyright (c) 2016 Xiaowen Lu
#-- Bioinformatics Group @ Wageningen University  --
#-- Modified 2016 Marc Chevrette
#-- Genetics @ UW-Madison
#-- This is the module to calculate the distance between each CLP pair

import sys
from munkres import Munkres
import math
import itertools
import pandas as pd
import numpy as np



def get_distance(DM, compd1, compd2, index1, index2):
    DM_input = open(DM, 'r').read().split('\n')
    index = [0,5]
    domain_name = []
    for l in DM_input:
        ## MC TROUBLESHOOTING ADDED HERE
        #raise Exception(l.split(",")[0].split("|")[3]);
        if l.split(",")[0].split("|")[0] is not '':
            domain = "|".join([l.split(",")[0].split("|")[i] for i in index])
            domain_name.append(domain)
            #print domain
            ## END TROUBLESHOOTING
    domain_index = dict(zip(domain_name, range(0,len(domain_name))))
    A = ['%s|%s' % t for t in zip([compd1]*len(index1), index1)]
    B = ['%s|%s' % t for t in zip([compd2]*len(index2), index2)]

    output = []
    
    for i in range(len(A)):
        A_i = A[i]
        B_i = B[i]
        
        row_index = domain_index.get(A_i)
        col_index = domain_index.get(B_i)
        ## MC-Add more troubleshooting here
        if row_index is not None and col_index is not None:
            if row_index <= col_index:
                row = DM_input[col_index]
                col = row_index
            else:
                row = DM_input[row_index]
                col = col_index
            score_list = row.split(",")
            # score_list.pop()
            score = 1-float(score_list[col+1])
            output.append(score)
            return output


        
def generate_BGCs_DMS(seq_simScore, cluster, pseudo_aa):

    input = open(cluster, 'r').read().split('>')[1:]
    cluster_id = [i.split('\n')[0] for i in input]
    pseudo_seq = [i.split('\n')[1] for i in input]
    cluster_seq = dict(zip(cluster_id, pseudo_seq))



    BGCs = {}
    """BGCs -- dictionary of this structure:  BGCs = {'cluster_name_x': { 'general_domain_name_x' : ['specific_domain_name_1',
     'specific_domain_name_2'] } }
    - cluster_name_x: cluster name (can be anything)
    - general_domain_name_x: PFAM ID, for example 'PF00550'
    - specific_domain_name_x: ID of a specific domain that will allow to you to map it to names in DMS unequivocally
     (for example, 'PF00550_start_end', where start and end are genomic positions)."""
    for id in cluster_seq.keys():
        cluster_i = {}

        seq = cluster_seq[id]
        seq_dict = {}
        for i in range(len(seq)):
            seq_dict[id+'_'+str(i+1)] = seq[i]

        domain_name = list(set(seq_dict.values()))
        for d in domain_name:
            key = d
            value = []
            for index, domain in seq_dict.items():
                if domain == d:
                    value.append(index)
            cluster_i[key] = value
        BGCs[id] = cluster_i



    DMS = {}
    """DMS -- dictionary of this structure: DMS = {'general_domain_name_x': { ('specific_domain_name_1',
    'specific_domain_name_2'): (sequence_identity, alignment_length), ... }   }
        - general_domain_name_x: as above
        - ('specific_domain_name_1', 'specific_domain_name_2'): pair of specific domains, sorted alphabetically
        - (sequence_identity, alignment_length): sequence identity and alignment length of the domain pair"""

    aa_list = [a for a in open(pseudo_aa, 'r').read().split('\n')[1:] if a != '']
    AA = [m.split('\t')[1] for m in aa_list]

    for aa in AA:
        domain_w_aa = []
        for i in BGCs.keys():
            cluster_i = BGCs[i]
            cluster_i_aa = cluster_i.keys()
            if aa in cluster_i_aa:
                domain_w_aa = domain_w_aa + cluster_i[aa]

        aa_pair_list = [tuple(sorted(list(i))) for i in list(itertools.combinations(domain_w_aa, 2))]
        aa_pair_dict = {}
        for p in aa_pair_list:
            compd1a = p[0].split('_')
            index1 = ['A'+ compd1a.pop()]
            compd1 = '_'.join(compd1a)
            compd2a = p[1].split('_')
            index2 = ['A'+ compd2a.pop()]
            compd2 = '_'.join(compd2a)

            ## MC-Add some troubleshooting here
            score = 0
            #score = get_distance(DM = seq_simScore, compd1 = compd1, compd2 = compd2, index1 = index1, index2 = index2)[0]
            s = get_distance(DM = seq_simScore, compd1 = compd1, compd2 = compd2, index1 = index1, index2 = index2)
            if s is not None:
                score = s[0]
            ## End troubleshooting
            aa_pair_dict[p] = (score, 0)

        DMS[aa] = aa_pair_dict


    return BGCs, DMS, cluster_seq



def calculate_GK(A, B, nbhood): #nbhood hardcoded in main
    # calculate the Goodman-Kruskal gamma index
    #
    #
    GK = 0.
    if len(set(A) & set(B)) > 1:
        ## Original
        #pairsA = set( [(A[i],A[j]) for i in xrange(len(A)-nbhood) for j in xrange(i+1,i+nbhood)] )
        #pairsB = set( [(B[i],B[j]) for i in xrange(len(B)-nbhood) for j in xrange(i+1,i+nbhood)] )
        ## MC Edit NOT YET WORKING
        Alist = [A[i:i+2] for i in range(0, len(A), 2)]
        Blist = [B[i:i+2] for i in range(0, len(B), 2)]
        #raise Exception()
        byorfA = []
        #print "Alist range: " + str(len(Alist)-nbhood)
        #for x in xrange(len(Alist)-nbhood):
        #    print "heres an a: " + str(x)
        #print "xr: " + str(xrange(len(Alist)-nbhood))
        #print "len: " + str(len(Alist))
        #print "nh: " + str(nbhood)
        ap = 0
        bp = 0
        for i in xrange(len(Alist)-nbhood):
            for j in xrange(i+1,i+nbhood):
        #        if i == 0 and j == 0:
        #            print "Alist: " + str(Alist[i])
                if Alist[i][0] is Alist[j][0]:
                    ap += 1
                    if int(Alist[i][0]) in byorfA:
                           byorfA[ int(Alist[i][0]) ] = set( [ byorfA[ int(Alist[i][0]) ], (Alist[i][1], Alist[j][1]) ] )
                    else:
                           byorfA = set( [ (Alist[i][1], Alist[j][1]) ] )
        byorfB = []
        #print "Blist range: " + str(len(Blist)-nbhood)
        #for x in xrange(len(Blist)-nbhood):
        #    print "heres a b: " + str(x)
        #print "xr: " + str(xrange(len(Blist)-nbhood))
        #print "len: " + str(len(Blist))
        #print "nh: " + str(nbhood)
        #raise Exception()
        for i in xrange(len(Blist)-nbhood):
            for j in xrange(i+1,i+nbhood):
        #        if i == 0 and j == 0:
        #            print "Blist: " + str(Blist[i])
                if Blist[i][0] is Blist[j][0]:
                    bp += 1
                    if int(Blist[i][0]) in byorfB:
                           byorfB[ int(Blist[i][0]) ] = set( [ byorfB[ int(Blist[i][0]) ], (Blist[i][1], Blist[j][1]) ] )
                    else:
                           byorfB = set( [ (Blist[i][1], Blist[j][1]) ] )
        if len(byorfA) == 0 or len(byorfB) == 0:
            GK = 0 ## THIS NEEDS CHANGING
        else:
        #for j in xrange(len(Alist)+nbhood):
        #    print str([Alist[j]])
        #raise Exception()
        fullpairsA = set( [ ([Alist[i]],[Alist[j]]) for i in xrange(len(Alist)-nbhood) for j in xrange(i+1,i+nbhood) ] )
        fullpairsB = set( [ ([Blist[i]],[Blist[j]]) for i in xrange(len(Blist)-nbhood) for j in xrange(i+1,i+nbhood) ] )
        #fullpairsA = set( [[(''.join(Alist[i]),''.join(Alist[j]))] for i in xrange(len(Alist)-nbhood) for j in xrange(i+1,i+nbhood)] )
        #fullpairsB = set( [[(Blist[i],Blist[j])] for i in xrange(len(Blist)-nbhood) for j in xrange(i+1,i+nbhood)] )
        
        raise Exception(fullpairsA)
        #pairsA = []
        #for pa in fullpairsA:
        #    if pa[0][0] is pa[1][0]:
        #        pairsA.append( [(pa[1][1], pa[1][1])])
        #pairsB = []
        #for pb in fullpairsB:
        #    if pb[0][0] is pb[1][0]:
        #        pairsB.append( [(pb[1][1], pb[1][1])])
        #pairsA = set(pairsA)
        #pairsB = set(pairsB)
        #raise Exception(pairsA)
        ## End edit
        allPairs = set(list(pairsA) + list(pairsB))
        Ns, Nr = 0.,0.
        for p in allPairs:
            if p in pairsA and p in pairsB: Ns += 1
            elif p in pairsA and tuple(p[::-1]) in pairsB: Nr += 1
            elif tuple(p[::-1]) in pairsA and p in pairsB: Nr += 1
            else: pass
        if (Nr + Ns) == 0:
            gamma = 0
        else:
            gamma = abs(Nr-Ns) / (Nr+Ns)
        GK = (1+gamma)/2.
    return GK




def cluster_distance(A, B, nbhood):

    #key is name of GC, values is list of specific pfam domain names
    clusterA = BGCs[A] #will contain a dictionary where keys are pfam domains, and values are domains that map to a specific sequence in the DMS variable
    clusterB = BGCs[B] #{ 'general_domain_name_x' : ['specific_domain_name_1', 'specific_domain_name_2'], etc }
    #A and B are lists of pfam domains

    try:
        #calculates the intersect
        Jaccard = len(set(clusterA.keys()) & set(clusterB.keys())) / \
              float( len(set(clusterA.keys())) + len(set(clusterB.keys())) \
              - len(set(clusterA.keys()) & set(clusterB.keys())))
    except ZeroDivisionError:
        print "Zerodivisionerror during the Jaccard distance calculation. Can only happen when one or more clusters contains no pfam domains."
        print "keys of clusterA", A, clusterA.keys()
        print "keys of clusterB", B, clusterB.keys()

    intersect = set(clusterA.keys() ).intersection(clusterB.keys()) #shared pfam domains
    not_intersect = []
    #DDS: The difference in abundance of the domains per cluster
    #S: Max occurence of each domain

    DDS,S = 0,0
    SumDistance = 0
    pair = ""

    for domain in set(clusterA.keys() + clusterB.keys()):
        if domain not in intersect:
            not_intersect.append(domain)

    for unshared_domain in not_intersect: #no need to look at seq identity or anchors, since these domains are unshared
        #for each occurence of an unshared domain do DDS += count of domain and S += count of domain
        dom_set = []
        try:
            dom_set = clusterA[unshared_domain]
        except KeyError:
            dom_set = clusterB[unshared_domain]

        DDS += len(dom_set)
        S += len(dom_set)


    for shared_domain in intersect:
        seta = clusterA[shared_domain]
        setb = clusterB[shared_domain]
        # print seta, setb

        if len(seta+setb) == 2: #The domain occurs only once in both clusters
            pair = tuple(sorted([seta[0],setb[0]]))
            ## MC-Add troubleshooting
            try:
                SumDistance = 1-DMS[shared_domain][pair][0]
                # print 'SumDistance1', SumDistance
            except:
                SumDistance = 1
            #except KeyError:
            #    print "KeyError on", pair
            #
            #    errorhandle = open(str(A)+".txt", 'w')
            #    errorhandle.write(str(pair)+"\n")
            #    errorhandle.write(str(DMS[shared_domain]))
            #    errorhandle.close()
            ##End troubleshooting
            S += max(len(seta),len(setb))
            DDS += SumDistance

        else:                   #The domain occurs more than once in both clusters
            # accumulated_distance = 0

            DistanceMatrix = [[1 for col in range(len(setb))] for row in range(len(seta))]
            # print DistanceMatrix
            for domsa in range(len(seta)):
                # print seta[domsa]
                for domsb in range(domsa, len(setb)):
                    # print setb[domsb]
                    pair = tuple(sorted([seta[domsa], setb[domsb]]))
                    ## MC-Add troubleshooting
                    #print pair
                    try:
                        Similarity = DMS[shared_domain][pair][0]
                        # print Similarity
                    except:
                        Similarity = 0
                        #except KeyError:
                    #    print "KeyError on", pair
                    #    errorhandle = open(str(B)+".txt", 'w')
                    #    errorhandle.write(str(pair)+"\n")
                    #    errorhandle.write(str(DMS[shared_domain]))
                    #    errorhandle.close()
                    ## End troubleshooting
                    
                    seq_dist = 1-Similarity
                    DistanceMatrix[domsa][domsb] = seq_dist


            #Only use the best scoring pairs
            Hungarian = Munkres()
            #print "DistanceMatrix", DistanceMatrix
            BestIndexes = Hungarian.compute(DistanceMatrix)
            # print "BestIndexes", BestIndexes
            accumulated_distance = sum([DistanceMatrix[bi[0]][bi[1]] for bi in BestIndexes])
            # print "accumulated_distance", accumulated_distance
            SumDistance = (abs(len(seta)-len(setb)) + accumulated_distance)  #diff in abundance + sequence distance
            # print 'SumDistance2', SumDistance


            S += max(len(seta),len(setb))
            DDS += SumDistance



    #  calculate the Goodman-Kruskal gamma index
    A_pseudo_seq = list(cluster_seq[A])
    B_pseudo_seq = list(cluster_seq[B])
    Ar = [item for item in A_pseudo_seq]
    Ar.reverse()
    GK = max([calculate_GK(A_pseudo_seq, B_pseudo_seq, nbhood = nbhood), calculate_GK(Ar, B_pseudo_seq, nbhood = nbhood)])

    DDS /= float(S)
    DDS /= float(S)
    DDS = math.exp(-DDS) #transform from distance to similarity score
    # print 'DDS', DDS



    # DDS = 1-DDS #transform into similarity

    Distance = 1 - (Jaccardw * Jaccard) - (DDSw * DDS) - (GKw * GK)
    Similarity_score = (Jaccardw * Jaccard) + (DDSw * DDS) + (GKw * GK)
    # Similarity_score = 1 - DDS
    if Distance < 0:
        print "negative distance", Distance, "DDS", DDS, pair
        print "Probably a rounding issue"
        print "Distance is set to 0 for these clusters"
        Distance = 0
    print A, B, Jaccard, GK, DDS
    return Similarity_score







def generate_distance_matrix(cluster_list, scale, nbhood, outfile):
    r = len(cluster_list)
    c = len(cluster_list)

    Dist = [[0 for x in range(r)] for y in range(c)]
    df = pd.DataFrame(Dist, index = cluster_list, columns = cluster_list)
    pairs = list(itertools.combinations(cluster_list, 2))

    for p in pairs:
        sim_score = cluster_distance(A = p[1],  B=p[0], nbhood=nbhood)
        rowname = p[1]
        colname = p[0]
        df.ix[rowname, colname] = 1- sim_score/scale

    df.to_csv(outfile)

    return df




## run the script to calculate the distance

global Jaccardw
global GKw
global DDSw
Jaccardw = 0.5
GKw = 0.25
DDSw = 0.25



#Please change the filenames according to where they are on your local saving
#seq_simScore = "/Users/Xiaowen/Documents/Pseudomonas_genomes_BGCs/phylogenetic_reconstruction/Pseudomonas_LP_all/sequence_similarity_score.txt"
#cluster = "/Users/Xiaowen/Documents/Pseudomonas_genomes_BGCs/phylogenetic_reconstruction/Pseudomonas_LP_all/Phylogeny_reconstruction_A_domain/domain_pseudoSeq/pseudomonas_domain_pseudoSeq.fasta"
#pseudo_aa = "/Users/Xiaowen/Documents/Pseudomonas_genomes_BGCs/phylogenetic_reconstruction/Pseudomonas_LP_all/Phylogeny_reconstruction_A_domain/Clade_PhyloNode_Adomain_all_for_alignment.txt"
#outfile = "/Users/Xiaowen/Desktop/dist.txt"
#tree_outfile = "/Users/Xiaowen/Desktop/tree_upgmma_all_clade.nwk"

## NEWLY MADE INPUT FILES
seq_simScore = "./seqsim_matrix.txt"
cluster = "./corenrps.faa"
pseudo_aa = "./corenrps.clade.txt"

## OUTPUT FILES
outfile = "mc_dist.txt"
tree_outfile = "mc_tree_upgmma_all_clade.nwk"

BGCs, DMS, cluster_seq = generate_BGCs_DMS(seq_simScore = seq_simScore, cluster = cluster, pseudo_aa = pseudo_aa)
cluster_list = cluster_seq.keys()
dist_score_assembly_line = generate_distance_matrix(cluster_list, scale = 1, nbhood = 3, outfile = outfile)



#-- Plot the tree
import Bio.Phylo
from Bio.Phylo.TreeConstruction import _Matrix, _DistanceMatrix
from Bio.Phylo.TreeConstruction import _Matrix, _DistanceMatrix
from Bio.Phylo.TreeConstruction import DistanceTreeConstructor
names = cluster_list
score = [s for s in open(outfile, 'r').read().split('\n')[1:] if s != '']
matrix = []
for i in range(len(score)):
    input_i = score[i].split(',')[1:(i+2)]
    input_i_int = [float(n) for n in input_i]
    matrix.append(input_i_int)
m = _DistanceMatrix(names, matrix)

constructor = DistanceTreeConstructor()
tree1 = constructor.upgma(m)
Bio.Phylo.draw_ascii(tree1)
Bio.Phylo.write(tree1, tree_outfile, 'newick')
